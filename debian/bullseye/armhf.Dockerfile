# Copyright 2020-2025 IQRF Tech s.r.o.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

FROM iqrftech/debian-debootstrap:armhf-bullseye

LABEL maintainer="Roman Ondráček <roman.ondracek@iqrf.com>"

ENV DEBIAN_FRONTEND="noninteractive"

RUN apt-get update \
 && apt-get install -y gnupg2 \
 && apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 0E98404D386FA1D9 6ED0E7B82643E131

RUN echo "deb http://deb.debian.org/debian/ bullseye main contrib non-free\n\
deb http://deb.debian.org/debian/ bullseye-updates main contrib non-free\n\
deb http://deb.debian.org/debian-security bullseye-security main\n\
deb http://deb.debian.org/debian bullseye-backports main" > /etc/apt/sources.list

RUN apt-get update \
  && apt-get upgrade -y \
  && apt-get install --no-install-recommends -t bullseye-backports -y \
     build-essential ca-certificates curl dirmngr devscripts dh-make fakeroot \
     git git-buildpackage gnupg2 lsb-release openssh-client python3-debian \
     python3-pip python3-setuptools python3-venv python3-wheel rsync \
  && curl -sSLo /usr/share/keyrings/iqrf.gpg https://repos.iqrf.org/apt.gpg \
  && echo "deb [signed-by=/usr/share/keyrings/iqrf.gpg] https://repos.iqrf.org/debian bullseye stable" | tee /etc/apt/sources.list.d/iqrf.list \
  && apt-get update \
  && apt-get install --no-install-recommends -y dch-add-distro \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*
