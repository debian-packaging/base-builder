# Copyright 2020-2025 IQRF Tech s.r.o.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

FROM iqrftech/debian-debootstrap:i386-buster

LABEL maintainer="Roman Ondráček <roman.ondracek@iqrf.com>"

ENV DEBIAN_FRONTEND="noninteractive"

RUN echo "deb http://archive.debian.org/debian/ buster main contrib non-free\n\
deb http://archive.debian.org/debian/ buster-updates main contrib non-free\n\
deb http://deb.debian.org/debian-security buster/updates main\n\
deb http://archive.debian.org/debian/ buster-backports main" > /etc/apt/sources.list

RUN apt-get update \
  && apt-get upgrade -y \
  && apt-get install --no-install-recommends -t buster-backports -y \
     debhelper devscripts gnupg2 \
  && apt-get install --no-install-recommends -y \
     build-essential ca-certificates curl dh-make dirmngr fakeroot \
     git git-buildpackage lsb-release openssh-client python3-debian \
     python3-pip python3-setuptools python3-wheel python3-venv rsync \
  && curl -sSLo /usr/share/keyrings/iqrf.gpg https://repos.iqrf.org/apt.gpg \
  && echo "deb [signed-by=/usr/share/keyrings/iqrf.gpg] https://repos.iqrf.org/debian buster stable" | tee /etc/apt/sources.list.d/iqrf.list \
  && apt-get update \
  && apt-get install --no-install-recommends -y dch-add-distro \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*
